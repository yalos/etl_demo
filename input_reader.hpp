//read and formats the input

#include "data_exchange.hpp"
#include "tokenizer.hpp"

class input_reader : private tokenizer
{
private:
	std::ifstream m_input_stream;
	std::vector<std::string> m_header;
public:
	//construction/destruction
	input_reader( const std::string& file_name, const char separator )
		: tokenizer(separator)
	{
		m_input_stream.exceptions ( std::ifstream::failbit );
		open_and_read_header( file_name );
	}

	//functionality
	bool next( data_exchange& output )
	{
		LOG_MSG( INFO, "About to read a line..." );

		size_t bound;
		std::string line;
		std::vector<std::string> tokens;

		output.clear();

		try
		{
			//1. read
			std::getline( m_input_stream, line );
			
			//2. tokenize
			tokens = tokenize(line);
			
			//3. Validate
			if ( 3 > m_header.size() ) { throw std::runtime_error( std::string("malformed file: line too short (") + line + ")" ); }//todo: here could log and continue

			
			//interpret
			output.m_database = tokens[0];
			output.m_table_name = tokens[1];
			
			bound = std::min( m_header.size(), tokens.size() );
			
			if ( m_header.size() != tokens.size() )
			{
				LOG_MSG( INFO, "Found a line that is not on the same size as the headers. Might truncate!" );
			}

			std::string log_line("Line has this data: ");

			for ( size_t i = 2; i < bound; ++i )
			{
				output.m_statement_data.push_back( std::make_pair( m_header[i], tokens[i] ) );
				log_line.append( m_header[i]+":"+ tokens[i] + " ");
			}
			LOG_MSG( DEBUG, log_line );
			LOG_MSG( INFO, "Line OK." );
			return true;
		}
		catch ( std::ifstream::failure& )
		{
			LOG_MSG( INFO, "Hit the EOF." );
			return false;
		}
	}
private:
	//internal logic
	void open_and_read_header( const std::string& file_name ) //throw(std::runtime_error)
	{
		LOG_MSG( INFO, "About to read the input header..." );

		std::string header;

		//1. Open
		try
		{
			m_input_stream.open( file_name );
			std::getline( m_input_stream, header );
		}
		catch ( std::ifstream::failure& )
		{
			throw std::runtime_error(std::string("cannot read the file: ") + file_name);
		}

		//2. Tokenize
		m_header = tokenize( header );
		
		//3. Validate

		//at least database|table|column0
		if ( 3 > m_header.size() ) { throw std::runtime_error( "malformed file: header too short" ); }
		//1st token is database
		if ( m_header[0] != "databasename" ){ throw std::runtime_error( "malformed file: expecting databasename as first column" ); }
		//2nd token is table
		if ( m_header[1] != "tablename" ){ throw std::runtime_error( "malformed file: expecting tablename as second column" ); }

		LOG_MSG( DEBUG, std::string("Working on: database=") + m_header[0] + " table=" + m_header[1] );

		LOG_MSG( INFO, "Input header OK." );
	}
};