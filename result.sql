------------------------------
begin tran
-------------------------------- storedata ----------------------------
if not exists (SELECT * from storedata where storeid=1 and storename='store1')
insert into storedata(storeid,storename,storedesc,store1,store2)
values(1,'store1','store desc1','A','F')

if not exists (SELECT * from storedata where storeid=2 and storename='store2')
insert into storedata(storeid,storename,storedesc,store1,store2)
values(2,'store2','store desc2','B','G')

if not exists (SELECT * from storedata where storeid=3 and storename='store3')
insert into storedata(storeid,storename,storedesc,store1,store2)
values(3,'store3','store desc3','C','H')

if not exists (SELECT * from storedata where storeid=4 and storename='store4')
insert into storedata(storeid,storename,storedesc,store1,store2)
values(4,'store4','store desc4','D','I')

if not exists (SELECT * from storedata where storeid=5 and storename='store5')
insert into storedata(storeid,storename,storedesc,store1,store2)
values(5,'store5','store desc5','E','J')
-------------------------------- storedata ----------------------------

if @@error = 0 
begin
commit tran
print '>> Insert into storedata completed. <<<'
end
else
begin
rollback tran
print '>> Insert into storedata failed. <<<'
end
