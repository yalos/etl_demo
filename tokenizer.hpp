//implements a tokenizer

#pragma once

class tokenizer
{
private:
		const char m_separator;
public :
	tokenizer( const char separator ) : m_separator( separator ){}
protected:
	std::vector<std::string> tokenize( const std::string& what )
	{	
		std::vector<std::string> result;
		std::size_t offset = 0;
		std::size_t match;
		
		while ( std::string::npos !=  ( match = what.find( m_separator, offset ) ) )
		{
			result.push_back( what.substr( offset, match-offset ) );
			offset = match+1;
		}

		result.push_back( what.substr( offset, what.size()-offset ) );

		return result;
	}
};