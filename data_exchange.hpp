//data to be exchanged between input and output

#pragma once

struct data_exchange
{
	typedef std::vector< std::pair<std::string, std::string> > key_val_type;

	enum type_info
	{
		TI_NUMBER,
		TI_STRING
	};

	std::string m_database;
	std::string m_table_name;
	
	key_val_type m_statement_data;

	void log()
	{
		std::cout<< "-------------------------data-line-------------------------" <<std::endl;
		std::cout <<  m_database << ":" << m_table_name << std::endl;
		for ( key_val_type::const_iterator i = m_statement_data.begin(), e = m_statement_data.end(); i != e; ++i )
		{
			std::cout << i->first << ":" << i->second << std::endl;
		}
		std::cout<< "-----------------------------------------------------------" <<std::endl;

	}

	type_info typeof( const size_t idx )
	{
		type_info result( TI_STRING );
		char* end_pos;
		if ( idx >= m_statement_data.size() ) { return result; }
		double dummy = strtod( m_statement_data[idx].second.c_str() ,&end_pos);

		if ( 0 == dummy && 0 != *end_pos ) { result = TI_STRING; }
		else { result = TI_NUMBER; }
		return result;
	}

	const std::string get_value_for( const std::string& key )
	{
		for ( size_t i = 0; i < m_statement_data.size(); ++i )
		{
			if ( m_statement_data[i].first == key )
			{
				if ( TI_STRING == typeof( i ) ) { return std::string("'") + m_statement_data[i].second + "'"; }
				else { return m_statement_data[i].second; }
			}
		}
		throw std::runtime_error( std::string( "data exchange: cannot find value for: " ) + key );
	}

	void clear()
	{
		m_database.clear();
		m_table_name.clear();
		m_statement_data.clear();
	}
};