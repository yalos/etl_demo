// etl.cpp : Defines the entry point for the console application.
//

#include "precompiled_header.hpp"

#include "configuration.hpp"
#include "logger.hpp"
#include "input_reader.hpp"
#include "output_writer.hpp"
#include "data_exchange.hpp"

void print_usage()
{
	std::cout << "usage:" << std::endl << std::endl;
	std::cout << "GenerateInsertSql InputFile OutputFile [-execute] [-debug]" << std::endl << std::endl;
	std::cout << "where: " << std::endl << std::endl;
	std::cout << "InputFile is the file to be processed (csv)" << std::endl;
	std::cout << "OutputFile is the file to be generated (sql)" << std::endl;
	std::cout << "[execute] if present the generated file will be executed aganst the data store" << std::endl;
	std::cout << "[debug] if present generates verbose log file" << std::endl;
}

void log_error( std::runtime_error& e )
{
	std::cout << e.what() << std::endl;
}

int main(int argc, char* argv[])
{
	data_exchange line;

	if ( !configuration::instance().load( argc, argv ) )
	{
		print_usage();
		return -1;
	}

	configuration& cfg = configuration::instance();

	logger::instance().verbose( cfg.verbose_log() );

	LOG_MSG( INFO, "Start compilation" );
	LOG_MSG( DEBUG, std::string("Called for the file: ") + cfg.input_file_name() );
	LOG_MSG( DEBUG, std::string("Should generate the file: ") + cfg.output_file_name() );

	try
	{
		input_reader reader( cfg.input_file_name(), cfg.separator() );
		output_writer writer( cfg.output_file_name(), cfg.separator(), cfg.keys(), cfg.insert_limit() );

		while ( reader.next( line ) )
		{
			writer.put( line );
		}

	} 
	catch ( std::runtime_error& e ) 
	{
		log_error( e );
		LOG_MSG( INFO, std::string("Terminating due to errors: ")+e.what()  );
		return -1;
	}

	if ( cfg.should_execute() )
	{
		LOG_MSG( INFO, "About to execute the generated script" );
		//here call the sdk
	}

	LOG_MSG( INFO, "All done. Exit with success." );

	return 0;
}

