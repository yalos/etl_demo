//logs messages to a file

#pragma once

#define INFO logger::LOG_INFO
#define DEBUG logger::LOG_DEBUG
#define LOG_MSG(x, y) logger::instance().log( ( x ), ( y ) );

//singleton
class logger
{
public:
	enum log_level
	{
		LOG_INFO = 0,
		LOG_DEBUG = 1
	};
private:
	log_level m_level;
	bool m_state; //true if open
	std::ofstream m_log_stream;
private:
	//construction/destruction
	logger() 
		: m_level( LOG_INFO )
		, m_state(false)
	{
		open_log();
	}

	logger( const logger& other )
	{
		throw std::runtime_error("logger:: no copy semantics");
	}

	logger& operator=( const logger& other )
	{
		throw std::runtime_error("logger:: no copy semantics");
	}

	~logger()
	{
		if ( m_state )
		{
			m_log_stream.flush();
			m_log_stream.close();
		}
	}
public:
	//functionality
	void verbose( bool state )
	{
		m_level = state ? LOG_DEBUG : LOG_INFO;
	}

	void log ( log_level level, const std::string& what )
	{
		if ( m_level < level ) { return; }
		
		time_t now = time(0);
		char* dt = ctime(&now);
		dt[strlen(dt)-1]=0;

		m_log_stream << dt << ": " <<what << std::endl;
	}
private:
	//internal mechanics
	bool open_log()
	{
		time_t rawtime;
		struct tm * timeinfo;
		char buffer [1024];

		time (&rawtime);
		timeinfo = localtime (&rawtime);

		strftime (buffer,1024,"log\\GenerateInsertSql_%y%m%d_%H%M%S.log",timeinfo);
	
		m_log_stream.open( buffer );
		m_state = m_log_stream.is_open();
		return m_state;
	}
public:
	//singleton access
	static logger& instance()
	{
		static logger the_instance;
		return the_instance;
	}
};