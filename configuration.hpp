//handles command line parameters

//singleton
class configuration
{
private:
	//data
	std::string m_input_file_name;
	std::string m_output_file_name;
	bool m_should_execute;
	bool m_verbose_log;

	//added extra
	char m_separator;
	size_t m_insert_limit;
	std::string m_keys;
private:
	//construction/destruction
	configuration()
		: m_should_execute(false)
		, m_verbose_log(false)
		, m_separator( '|' )
		, m_insert_limit( 5 )
	{
	}

	configuration( const configuration& other )
	{
		throw std::runtime_error("configuration:: no copy semantics");
	}

	configuration& operator=( const configuration& other )
	{
		throw std::runtime_error("configuration:: no copy semantics");
	}

private:
	//internal mechanics
public:
	//singleton access
	static configuration& instance()
	{
		static configuration the_instance;
		return the_instance;
	}
	//functionality
	bool load(int argc, char* argv[])
	{
		static std::string execute_str("-execute");
		static std::string debug_str("-debug");

		if ( 3 > argc || 6 < argc ) { return false; }
		m_input_file_name = argv[1];
		m_output_file_name = argv[2];
		for ( int i = 3; i < argc; ++i )
		{
			if ( execute_str == argv[i] ) { m_should_execute = true; }
			else if ( debug_str == argv[i] ) { m_verbose_log = true; }
			else { m_keys=argv[i]; }//everything else is interpreted as keys
		}
		return true;
	}

	const std::string& input_file_name() const { return m_input_file_name; }
	
	const std::string& output_file_name() const { return m_output_file_name; }
	
	const std::string& keys() const { return m_keys; }	

	const bool verbose_log() const { return m_verbose_log; }

	const bool should_execute() const { return m_should_execute; }

	const char separator() const { return m_separator; }

	const size_t insert_limit() const { return m_insert_limit; }
};