//generates the file containing the statement

#include "data_exchange.hpp"
#include "tokenizer.hpp"

class output_writer : private tokenizer
{
private:
	std::ofstream m_output_stream;
	std::string m_database_name;
	std::string m_table_name;

	const size_t m_insert_limit;

	std::vector<std::string> m_keys;
public:
	//construction/destruction
	output_writer( const std::string& file_name, const char separator, const std::string& keys, const size_t insert_limit )
		: tokenizer( separator )
		, m_database_name("")
		, m_table_name("")
		, m_insert_limit( insert_limit )
	{
		m_keys = tokenize( keys );

		m_output_stream.exceptions ( std::ifstream::failbit );
		
		try
		{		
			m_output_stream.open( file_name );
		}
		catch ( std::ifstream::failure& )
		{
			throw std::runtime_error( std::string("cannot write the file: ") + file_name );
		}
	}

	~output_writer()
	{
		//write footer in the case there was some data
		if ( !m_table_name.empty() )
		{
			write_footer();
		}
		m_output_stream.flush();
	}

//all that follows is dialect.


	//functionality
	void put( data_exchange& what )
	{
		LOG_MSG( INFO, "About to write a line." );
		//1. first time the header should be written
		if ( m_database_name.empty() )
		{
			m_database_name = what.m_database;
			m_table_name = what.m_table_name;
			write_header();
		} else
		{
			m_output_stream << std::endl;
		}

		//2. add test
		std::stringstream test;

		test << "if not exists (SELECT * from " << what.m_table_name;
		std::string prepend = " where ";
		for ( size_t i = 0; i < m_keys.size(); ++i )
		{
			try
			{
				const std::string& key = m_keys[i];
				const std::string val = what.get_value_for( key );

				test << prepend << key << "=" << val;
			} catch ( std::runtime_error& e )
			{
				LOG_MSG( INFO, "Found a malformed line. Will skip." );
				LOG_MSG( DEBUG, std::string("the error was: ") + e.what() );
				return;
			}
			prepend = " and ";

		}
		test << ")" << std::endl;

		//3. add insert
		std::stringstream insert;
		std::stringstream values;

		insert << "insert into " << m_table_name << "(";
		values << "values(";
		prepend = "";
		size_t idx = 0;
		for ( data_exchange::key_val_type::const_iterator i = what.m_statement_data.begin(), e = what.m_statement_data.end(); i != e; ++i )
		{
			if ( idx >= m_insert_limit ) { break; }
			insert << prepend << i->first;
			if ( data_exchange::TI_NUMBER == what.typeof(idx++) )
			{
				values << prepend << i->second;
			} else
			{
				values << prepend << "'" << i->second << "'";
			}	
			prepend = ",";
		}
		insert << ")" << std::endl;
		values << ")" << std::endl;

		m_output_stream << test.str() << insert.str() << values.str();
		LOG_MSG( DEBUG, "Will format as follows:" );
		LOG_MSG( DEBUG, test.str() );
		LOG_MSG( DEBUG, insert.str() );
		LOG_MSG( DEBUG, values.str() );
		LOG_MSG( INFO, "Write OK." );
	}
private:
	void write_header()
	{
		LOG_MSG( INFO, "Writing the header." );
		m_output_stream << "------------------------------" << std::endl;
		m_output_stream << "begin tran" << std::endl;
		m_output_stream << "-------------------------------- "<< m_table_name <<" ----------------------------" << std::endl;
	}

	void write_footer()
	{
		LOG_MSG( INFO, "Writing the footer." );
		m_output_stream << "-------------------------------- "<< m_table_name <<" ----------------------------" << std::endl;
		m_output_stream << std::endl;
		m_output_stream << "if @@error = 0 " << std::endl;
		m_output_stream << "begin" << std::endl;
		m_output_stream << "commit tran" << std::endl;
		m_output_stream << "print '>> Insert into storedata completed. <<<'" << std::endl;
		m_output_stream << "end" << std::endl;
		m_output_stream << "else" << std::endl;
		m_output_stream << "begin" << std::endl;
		m_output_stream << "rollback tran" << std::endl;
		m_output_stream << "print '>> Insert into storedata failed. <<<'" << std::endl;
		m_output_stream << "end" << std::endl;
	}
};